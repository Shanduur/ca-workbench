#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/wait.h>

#define WRITE 1
#define READ 0

void child(int recv[2], int send[2], int n)
{
    float result;
    int buf, rc, i;
    buf = 0;
    rc = 0;

    rc = close(recv[WRITE]);
    if (rc == -1)
    {
        perror("unable to close recv write");
        exit(EXIT_FAILURE);
    }

    rc = close(send[READ]);
    if (rc == -1)
    {
        perror("unable to close send read");
        exit(EXIT_FAILURE);
    }

    while (read(recv[READ], &buf, 1))
    {
        float f;
        f = (float)buf;
        result = pow((f*f - 12), 3)/(pow(n, 3)*pow(f, 3));

        char s_result[100];

        sprintf(s_result, "%f\n", result);

        rc = write(send[WRITE], &s_result, strlen(s_result));
        if (rc == -1)
        {
            perror("unable to write to send pipe");
            exit(EXIT_FAILURE);
        };
    }

    rc = close(recv[READ]);
    if (rc == -1)
    {
        perror("unable to close recv read");
        exit(EXIT_FAILURE);
    }

    rc = close(send[WRITE]);
    if (rc == -1)
    {
        perror("unable to close send write");
        exit(EXIT_FAILURE);
    }

    return;
}

void parent(int recv[2], int send[2], int n)
{
    float sum;
    int buf, rc, i;
    buf = 0;
    sum = 0.f;
    rc = 0;

    rc = close(recv[WRITE]);
    if (rc == -1)
    {
        perror("unable to close recv write");
        exit(EXIT_FAILURE);
    }

    rc = close(send[READ]);
    if (rc == -1)
    {
        perror("unable to close send read");
        exit(EXIT_FAILURE);
    }

    for (i = 1; i <= n; i++)
    {
        rc = write(send[WRITE], &i, 1);
        if (rc == -1)
        {
            perror("unable to write to send pipe");
            exit(EXIT_FAILURE);
        }
    }

    close(send[WRITE]);

    char s_result[100];
    memset(s_result, 0, strlen(s_result));
    while (read(recv[READ], &buf, 1))
    {
        if (buf != '\n')
        {
            char c[5];
            sprintf(c, "%c", buf);
            strcat(s_result, c);
        }
        else 
        {
            float result;
            result = atof(s_result);
            sum += result;
            memset(s_result, 0, strlen(s_result));
        }
    }

    rc = close(recv[READ]);
    if (rc == -1)
    {
        perror("unable to close recv read");
        exit(EXIT_FAILURE);
    }

    printf("sum: %f\n", sum);

    wait(NULL);

    return;
}

int main(int argc, char **argv)
{
    int rc, i, k, n;
    rc = 0;

    if (argc == 3)
    {
        n = atoi(argv[1]);
        k = atoi(argv[2]);
    }
    else
    {
        printf("proper usage: %s [N] [K]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    

    int data_pipe[2];
    int result_pipe[2];

    rc = pipe(data_pipe);
    if (rc == -1)
    {
        perror("unable to create pipe\n");
        exit(EXIT_FAILURE);
    }
    rc = pipe(result_pipe);
    if (rc == -1)
    {
        perror("unable to create pipe\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < k; i++)
    {
        int cpid = fork();
        if (cpid == -1)
        {
            perror("unable to fork\n");
            exit(EXIT_FAILURE);
        }
        else if (cpid == 0)
        {
            child(data_pipe, result_pipe, n);
            _exit(EXIT_SUCCESS);
        }
        else
        {
            continue;
        }
    }

    parent(result_pipe, data_pipe, n);
}
