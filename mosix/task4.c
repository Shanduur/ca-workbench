#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define WRITE 1
#define READ 0

int isPrime(int n)
{
    int flag, i;
    flag = 1;

    for (i = 2; i <= n / 2; i++)
    {
        if (n % i == 0)
        {
            flag = 0;
            break;
        }
    }

    if (n == 1)
    {
        return -1;
    }
    else
        return flag;
}

void child(int recv[2], int send[2])
{
    int buf, result, rc, i;
    buf = 0;
    rc = 0;
    char message[100];

    rc = close(recv[WRITE]);
    if (rc == -1)
    {
        perror("unable to close recv write");
        exit(EXIT_FAILURE);
    }

    rc = close(send[READ]);
    if (rc == -1)
    {
        perror("unable to close send read");
        exit(EXIT_FAILURE);
    }

    while (read(recv[READ], &buf, 1))
    {
        result = isPrime(buf);
        if (result == 1)
        {
            rc = write(send[WRITE], &buf, 1);
            if (rc == -1)
            {
                perror("unable to write to send pipe");
                exit(EXIT_FAILURE);
            };
        }
    }

    rc = close(recv[READ]);
    if (rc == -1)
    {
        perror("unable to close recv read");
        exit(EXIT_FAILURE);
    }

    rc = close(send[WRITE]);
    if (rc == -1)
    {
        perror("unable to close send write");
        exit(EXIT_FAILURE);
    }

    return;
}

void parent(int recv[2], int send[2], int start, int stop)
{
    int buf, rc, i, sum;
    buf = 0;
    rc = 0;
    sum = 0;

    rc = close(recv[WRITE]);
    if (rc == -1)
    {
        perror("unable to close recv write");
        exit(EXIT_FAILURE);
    }

    rc = close(send[READ]);
    if (rc == -1)
    {
        perror("unable to close send read");
        exit(EXIT_FAILURE);
    }

    for (i = start; i <= stop; i++)
    {
        rc = write(send[WRITE], &i, 1);
        if (rc == -1)
        {
            perror("unable to write to send pipe");
            exit(EXIT_FAILURE);
        }
    }

    close(send[WRITE]);

    while (read(recv[READ], &buf, 1))
    {
        sum += buf;
    }

    rc = close(recv[READ]);
    if (rc == -1)
    {
        perror("unable to close recv read");
        exit(EXIT_FAILURE);
    }

    printf("sum: %d\n", sum);

    wait(NULL);

    return;
}

int main(int argc, char **argv)
{
    int rc, i, children, start, stop;
    rc = 0;

    if (argc == 4)
    {
        start = atoi(argv[1]);
        stop = atoi(argv[2]);
        children = atoi(argv[3]);
    }
    else
    {
        printf("proper usage: %s [start] [stop] [children]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    

    int data_pipe[2];
    int result_pipe[2];

    rc = pipe(data_pipe);
    if (rc == -1)
    {
        perror("unable to create pipe\n");
        exit(EXIT_FAILURE);
    }
    rc = pipe(result_pipe);
    if (rc == -1)
    {
        perror("unable to create pipe\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < children; i++)
    {
        int cpid = fork();
        if (cpid == -1)
        {
            perror("unable to fork\n");
            exit(EXIT_FAILURE);
        }
        else if (cpid == 0)
        {
            child(data_pipe, result_pipe);
            _exit(EXIT_SUCCESS);
        }
        else
        {
            continue;
        }
    }

    parent(result_pipe, data_pipe, start, stop);
}
