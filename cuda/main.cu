#include <stdio.h>
#include <assert.h>
#include <time.h>

__global__ void add(
    float *a_mat, float *b_mat, 
    float *result, int rows, int cols)
{
    int row = blockIdx.x;
    int col = threadIdx.x;

    if (row < rows && col < cols)
        result[row*cols+col] = 
            a_mat[row*cols+col] + b_mat[row*cols+col];
}

int main(int argc, char **argv)
{
    int rows, cols;

    if (argc == 3)
    {
        srand(time(NULL));
        rows = atoi(argv[1]);
        cols = atoi(argv[2]);
        if (rows < 1 || cols < 1)
        {
            printf("arguments should be larger tha 0\n");
            exit(EXIT_FAILURE);
        }
    }
    else 
    {
        printf("to run application type: %s [rows] [cols] \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    float *h_a, *h_b, *d_a, *d_b;

    h_a = (float*)malloc(sizeof(float)*rows*cols);
    h_b = (float*)malloc(sizeof(float)*rows*cols);

    for (int i = 0; i < rows*cols; i++)
    {
        h_a[i] = 0.5 - (float)rand()/(float)RAND_MAX;
        h_b[i] = 0.5 - (float)rand()/(float)RAND_MAX;
    }

    if (rows < 15 && cols < 15) 
    {
        printf("\n------ MAT A ------\n\n");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                printf("%f, ", h_a[i*cols+j]);
            }
            printf("\n");
        }
        printf("\n");

        printf("------ MAT B ------\n\n");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                printf("%f, ", h_b[i*cols+j]);
            }
            printf("\n");
        }
        printf("\n");
    }

    cudaMalloc(&d_a, sizeof(float)*rows*cols);
    cudaMemcpy(d_a, h_a, sizeof(float)*rows*cols, cudaMemcpyHostToDevice);

    cudaMalloc(&d_b, sizeof(float)*rows*cols);
    cudaMemcpy(d_b, h_b, sizeof(float)*rows*cols, cudaMemcpyHostToDevice);

// ------------------------

    float *d_out, *h_out;

    h_out = (float*)malloc(sizeof(float)*rows*cols);
    cudaMalloc(&d_out, sizeof(float)*rows*cols);

// ------------------------

    add<<<rows, cols>>>(d_a, d_b, d_out, rows, cols);

// ------------------------

    cudaMemcpy(h_out, d_out, sizeof(float)*rows*cols, cudaMemcpyDeviceToHost);

    if (rows < 15 && cols < 15) 
    {
        printf("----- RESULT -----\n\n");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                printf("%f, ", h_out[i*cols+j]);
            }
            printf("\n");
        }
        printf("\n");
    }
}