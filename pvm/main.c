#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "pvm3.h"

#define PROGNAME "/home/pvm/pvm3/lab/main"

struct _Mat
{
    int cols;
    int rows;
    float *data;
};

typedef struct _Mat Mat;

Mat *createMatrix(int p_rows, int p_cols)
{
    assert(p_rows > 0 && p_cols > 0);

    Mat *mat;
    mat = (Mat *)malloc(sizeof(Mat));
    mat->cols = p_cols;
    mat->rows = p_rows;

    mat->data = (float *)malloc(sizeof(float) * p_cols * p_rows);

    for (size_t i = 0; i < p_rows * p_cols; i++)
    {
        mat->data[i] = 5 - (float)rand() / (float)(RAND_MAX)*10;
    }

    return mat;
}

void printMatrix(Mat *mat)
{
    printf("\n\n========================\n");
    for (size_t i = 0; i < mat->cols; i++)
    {
        for (size_t j = 0; j < mat->rows; j++)
        {
            printf("%f, ", mat->data[i * mat->cols + j]);
        }
        printf("\n");
    }
    printf("========================\n\n");
}

void parent(int argc, char** argv)
{
    int cols, rows;
    Mat *mat;

    if (argc == 3) {
        cols = atoi(argv[1]);
        rows = atoi(argv[2]);
    } else {
        printf("not enough arguments: app cols rows");
        exit(1);
    }

    mat = createMatrix(cols, rows);
    printMatrix(mat);

    struct pvmhostinfo *hostp;
    int result, rc, ilhost, ilarch, child_id, parent_id, part_size;

    parent_id = pvm_mytid();

    if (parent_id < 0)
    {
        pvm_perror("enroll");
        exit(1);
    }

    pvm_config(&ilhost, &ilarch, &hostp);

    part_size = (int)ceil((float)mat->cols * (float)mat->rows / (float)ilhost);

    for (int i = 0; i < ilhost; i++)
    {
        printf("hostname: %s\n", hostp[i].hi_name);
        rc = pvm_spawn(PROGNAME, 0, PvmTaskHost, hostp[i].hi_name, 1, &child_id);
        if (!rc)
            printf("Error creating child on %s\n", hostp[i].hi_name);

        pvm_initsend(PvmDataDefault);

        pvm_pkint(&part_size, 1, 1);

        for (size_t j = 0; j < part_size; j++)
        {
            pvm_pkfloat(&(mat->data[i * ilhost + j]), 1, 1);
        }

        pvm_send(child_id, 100);
    }

    result = 0;

    float min, max;
    min = FLT_MAX;
    max = FLT_MIN;

    while (result < ilhost)
    {
        float m_min, m_max;

        pvm_recv(-1, 200);
        pvm_upkfloat(&m_min, 1, 1);
        pvm_upkfloat(&m_max, 1, 1);

        min = (min > m_min) ? m_min : min;
        max = (max < m_max) ? m_max : max;

        result++;
    }

    printf("\n\n========================\n"
           "max: %f\n"
           "min: %f\n"
           "========================\n\n",
           max, min);

    pvm_exit();
}

void child()
{
    char buffer[100];

    int parent_id;
    parent_id = pvm_parent();

    pvm_recv(-1, 100);

    float m_min, m_max;
    m_min = FLT_MAX;
    m_max = FLT_MIN;

    int size;

    pvm_upkint(&size, 1, 1);

    assert(size > 0);

    float *p_data;
    p_data = (float *)malloc(sizeof(float) * size);

    for (size_t i = 0; i < size; i++)
    {
        pvm_upkfloat(&p_data[i], 1, 1);
    }

    for (size_t i = 0; i < size; i++)
    {
        m_min = (p_data[i] < m_min) ? p_data[i] : m_min;
        m_max = (p_data[i] > m_max) ? p_data[i] : m_max;
    }

    pvm_initsend(PvmDataDefault);

    pvm_pkfloat(&m_min, 1, 1);
    pvm_pkfloat(&m_max, 1, 1);
    pvm_send(parent_id, 200);

    pvm_exit();
}

int main(int argc, char** argv)
{
    if (pvm_parent() == PvmNoParent)
        parent(argc, argv);
    else
        child();
}
